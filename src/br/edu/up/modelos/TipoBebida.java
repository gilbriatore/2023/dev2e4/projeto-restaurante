package br.edu.up.modelos;

public enum TipoBebida {
	AGUA_COM_GAS,
	AGUA_SEM_GAS,
	CERVEJA,
	REFRIGERANTE,
	SUCO,
	VINHO_SECO,
	VINHO_SUAVE
}
